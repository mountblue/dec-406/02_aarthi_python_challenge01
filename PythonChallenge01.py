'''
Input Parameters
----------------
csv_filename : str
    a formatted string input of csv file name
search_datetime : str
    the formatted string with date and time as YYYY-MM-DD H:MMpm/ H:MMam

Functions
---------
find_open_restaurants(csv_filename, search_datetime):
    Prints the restaurants name open on the given date and time
TimeRangeChecker(startTime,endTime,nowTime):
    Checks weather the time is in the range or not and returns True or False
'''

import csv
from datetime import datetime
import time

def find_open_restaurants(csv_filename, search_datetime):
    #finding day of the given date
    try:
        week_day=(time.strftime("%A", time.strptime(search_datetime[0:10], "%Y-%m-%d")))
        #opening csv file
        with open(csv_filename) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            day=['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
            for row in csv_reader:
                #spliting the timings of each restaurant
                timings=row[1].split(' / ')
                for i in range (0,len(timings)):
                    # initializing result res as empty string
                    res = ""
                    #case1: Mon 10:30 pm - 11am
                    if(timings[i].count('-') == 1):
                        if(timings[i][0:3]==week_day[0:3]):
                            time_range=(timings[i][4:].split(' - '))
                            res=(row[0])
                    elif(timings[i].count('-')==2):
                        # case2: Mon-Thu 10 am - 11 pm
                        if(timings[i].count(',')==0):
                            week_range=timings[i][0:7].split('-')
                            time_range = (timings[i][8:].split(' - '))
                            for dayindex in range (day.index(week_range[0]),day.index(week_range[1])+1):
                                if(week_day[0:3]==day[dayindex]):
                                    res=(row[0])
                        # case3: Mon-Thu, Sat 10 am - 11 pm and Mon, Sat-Sun 10 am - 11 pm
                        elif(timings[i].count(',')==1):
                            week_range=(timings[i][0:12].split(', '))
                            time_range = (timings[i][13:].split(' - '))
                            if(week_range[0]==week_day[0:3] or week_range[1]==week_day[0:3]):
                                res=(row[0])
                            else:
                                if (len(week_range[0]) == 3):
                                    split_week_range=week_range[1].split('-')
                                    for dayindex in range(day.index(split_week_range[0]), day.index(split_week_range[1])+1):
                                        if (week_day[0:3] == day[dayindex]):
                                            res=(row[0])
                                elif(len(week_range[1]) == 3):
                                    split_week_range = week_range[0].split('-')
                                    for dayindex in range(day.index(split_week_range[0]),
                                                          day.index(split_week_range[1])+1):
                                        if (week_day[0:3] == day[dayindex]):
                                            res=(row[0])
                    else:
                        print(timings[i])
                    # Time range
                    start_time = time_range[0]
                    stop_time = time_range[1].strip()
                    if (start_time.count(':') == 0):
                        start_time = datetime.strptime(start_time, '%I %p')
                    else:
                        start_time = datetime.strptime(start_time, '%I:%M %p')
                    if (stop_time.count(':') == 0):
                        stop_time = datetime.strptime(stop_time, '%I %p')
                    else:
                        stop_time = datetime.strptime(stop_time, '%I:%M %p')
                    timeNow = datetime.strptime(search_datetime[11:], "%I:%M%p")
                    # Time Range Checker Function
                    if(TimeRangeChecker(start_time, stop_time, timeNow)==True and res!=""):
                        print(res)
                line_count+=1

    except FileNotFoundError:
        print('Incorrect File name')
        return True

    except ValueError:
        print('Incorrect date and time format')
        return True

    return False

def TimeRangeChecker(startTime,endTime,nowTime):
    if (startTime < endTime):
        return (nowTime >= startTime and nowTime <= endTime)
    else:  # Over midnight
        return (nowTime >= startTime or nowTime <= endTime)


flag=True
while(flag==True):
    print("Enter the date and time in YYYY-MM-DD H:MMpm/ H:MMam")
    given_date=input()
    file=input('Enter file name as .csv file')
    if(file[-4:]!='.csv'):
        print('incorrect file format')
        flag=True
    else:
        flag=find_open_restaurants(file,given_date)